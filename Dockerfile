FROM fedora:rawhide
RUN dnf install -y \
    shadow-utils \
    gcc \
    git \
    python3 \
    python3-devel \
    python3-pip \
    cargo \
    libffi-devel \
    python3-cryptography \
    python3-rpds-py \
    && \
    pip install --no-cache-dir ansible ansible-lint && \
    dnf clean all

RUN adduser ansible
USER ansible
WORKDIR /home/ansible

ENTRYPOINT [ "/usr/local/bin/ansible-lint" ]
