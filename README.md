# fresh-docker-ansible-lint
Check syntax from using newest ansible-lint package

Example usage:
```
# Run in the directory where the ansible file is
docker build . --tags fresh-docker-ansible-lint


docker run --volume /home/USER/ANSIBLE_FILES_DIRECTORY_HERE:/home/ansible fresh-docker-ansible-lint site.yml
```

Image is also on Docker Hub: https://hub.docker.com/repository/docker/bartlomiejkida/fresh-docker-ansible-lint
